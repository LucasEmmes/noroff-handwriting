# Handwriting recognition using OpenCV
In this project I adapted the code from [this example](https://docs.opencv.org/4.x/dd/d3b/tutorial_py_svm_opencv.html) to recognize  
characters from my own handwriting.

## Prerequisites
To run the program, you need to have numpy and opencv installed.  
To do this, run  
`pip install numpy`  
`pip install opencv-python`  

To run the script, simply do  
`python opencv.py`